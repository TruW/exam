/*
 * @Author William Trudel*/
package question3;

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DiceChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField money;
	private String choice;
	private TextField bet;
	private DiceGame game;
	
	public DiceChoice(TextField message,TextField wins,TextField losses,TextField money,String choice,TextField bet,DiceGame game) {
		this.message=message;
		this.wins=wins;
		this.losses=losses;
		this.money=money;
		this.choice=choice;
		this.bet=bet;
		this.game=game;
	}
	
	@Override
	public void handle(ActionEvent e) {
		int betNum=Integer.parseInt(bet.getText());
		if(betNum>game.getMoney()) {
			message.setText("Insufficient funds");
		}
		else {
			message.setText(game.playGame(choice,betNum));
			wins.setText("wins: "+game.getWins());
			losses.setText("losses: "+game.getLosses());
			money.setText("$"+game.getMoney());
		}
	}
}
