/*
 * @Author William Trudel*/
package question3;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class DiceApp extends Application {
	private DiceGame game=new DiceGame();
	public void start(Stage stage) {
		Group root = new Group(); 
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		HBox input=new HBox();
		Button even=new Button("even");
		Button odd=new Button("odd");
		TextField bet=new TextField("Enter bet");
		input.getChildren().addAll(even,odd,bet);
		
		HBox text=new HBox();
		TextField welcome=new TextField("Welcome!");
		welcome.setPrefWidth(200);
		TextField wins=new TextField("wins: "+game.getWins());
		TextField losses=new TextField("losses: "+game.getLosses());
		TextField money=new TextField("$"+game.getMoney());
		text.getChildren().addAll(welcome,wins,losses,money);
		
		VBox box=new VBox();
		box.getChildren().addAll(input,text);
		
		root.getChildren().add(box);
		
		DiceChoice cEven=new DiceChoice(welcome,wins,losses,money,"even",bet,game);
		even.setOnAction(cEven);
		DiceChoice cOdd=new DiceChoice(welcome,wins,losses,money,"odd",bet,game);
		odd.setOnAction(cOdd);
		
		stage.setTitle("Dice game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

