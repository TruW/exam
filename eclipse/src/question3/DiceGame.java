/*
 * @Author William Trudel*/
package question3;

import java.util.Random;

public class DiceGame {
	Random r=new Random();
	private int wins;
	private int losses;
	private int money;
	private int pick;
	
	public DiceGame() {
		this.wins=0;
		this.losses=0;
		this.money=250;
		this.pick=r.nextInt(2);
	}
	
	public int getWins() {
		return wins;
	}
	public int getLosses() {
		return losses;
	}
	public int getMoney() {
		return money;
	}
	
	public String playGame(String choice,int bet) {
		pick=r.nextInt(2);
		switch(pick) {
		case 0:
			if(choice.equals("even")) {
				wins++;
				money+=bet;
				return "Roll was even, you win";
			}
			else {
				losses++;
				money-=bet;
				return "Roll was even, you lose";
			}
		case 1:
			if(choice.equals("odd")) {
				wins++;
				money+=bet;
				return "Roll was odd, you win";
			}
			else {
				losses++;
				money-=bet;
				return "Roll was odd, you lose";
			}
			default:
				return "game error";
		}
	}
}
