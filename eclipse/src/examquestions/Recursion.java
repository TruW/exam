/*
 * @Author William Trudel*/
package examquestions;

public class Recursion {
	public static int recursiveCount(int[] numbers, int number) {
		int j=0;
		int count=0;
		int[] nNumbers=new int[numbers.length/2];
		
		
		if(numbers.length<10) {
			for(int i=0;i<numbers.length;i++) {
				if(numbers[i]<10) {
					count++;
				}
			}
			return count;
		}
		
		
		
		for(int i=0;i<numbers.length;i+=2) {
			
			if(numbers[i]>=number) {
				count++;
				nNumbers[j]=numbers[i];
				j++;
			}
		}
		return recursiveCount(nNumbers,number)+count;
	}
}
